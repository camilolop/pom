#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Formulario Popup Validation
El usuario debe poder ingresar al formulario los datos requeridos
cada campo del formulario realiza validaciones de obligatoriedad,
longitud y formato, el sistema debe presentar las validaciones respectivas para cada campo a traves de un globo informativo.



  @CasoExitoso
  Scenario: Diligenciamiento exitoso del formulario Popup Validation,
  no se presenta ningun mensaje de validación.
    Given Autentico en Colorlib con usuario "demo" y clave "demo"
    And Ingreso a la funcionalidad Forms Validation
    When Diligencio Formulario Popup Validation
   | valor1 | Golf | Tennis | http://www.valor1.com | valor1@gmail.com | valor1 | valor1 | 123456 | 123456 | -99.99 | 200.200.5.4 | 2012-12-01 | 2012/09/12 |
   
    Then Verifico ingreso exitoso
    
    @CasoAlterno
    Scenario: Diligenciamiento con errores del formulario popup Validation
    Se presenta globo Informativo indiciando error en el diligenciamiento
    
    Given Autentico en Colorlib con usuario "demo" y clave "demo"
    And Ingreso a la funcionalidad Forms Validation
  When Diligencio Formulario Popup Validation
    |  | Golf | Tennis | http://www.valor1.com | valor1@gmail.com | valor1 | valor1 | 123456 | 123456 | -99.99 | 200.200.5.4 | 2012-12-01 | 2012/09/12 |
     | valor1 | Choose a sport | Tennis | http://www.valor1.com | valor1@gmail.com | valor1 | valor1 | 123456 | 123456 | -99.99 | 200.200.5.4 | 2012-12-01 | 2012/09/12 |
   
   Then verificar que se presente globo informativo de validacion

	
@tag2
Scenario: Diligenciamiento exitoso del formulario	block validation,
se presenta mensaje de validacion.

Given Autentico en Colorlib con usuario "demo" y clave "demo"
 And Ingreso a la funcionalidad block Validation
When Diligencio Formulario block validation
|hpola|clopez@choucairtesting.com|hola1|hola1|06/05/2012|http://www.google.com|242|8|

Then Verifico ingreso exitoso1


  @casoalterno2
Scenario: Diligenciamiento exitoso del formulario	block validation,
no se presenta ningun mensaje de validacion.

Given Autentico en Colorlib con usuario "demo" y clave "demo"
 And Ingreso a la funcionalidad block Validation
When Diligencio Formulario block validation
||clopez@choucairtesting.com|hola1|hola1|06/05/2012|http://www.google.com|242|8|
 Then verificar que se presente globo informativo de validacion1