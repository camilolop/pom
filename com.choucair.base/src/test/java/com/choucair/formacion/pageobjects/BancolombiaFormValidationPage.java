package com.choucair.formacion.pageobjects;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import com.choucair.formacion.steps.BancolombiaValidationSteps;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class BancolombiaFormValidationPage extends PageObject{
BancolombiaValidationSteps bancolombiaValidationSteps;
	//Que deseas simular valor 0
		@FindBy(xpath="//*[@id=\'sim-detail\']/form/div[2]/select")
		public WebElementFacade tiposim;
		
		//fecha
				@FindBy(name="dateFechaNacimiento")
				public WebElementFacade fechanac;
				
				
				//Que deseas tasa 0 tasa fija
				@FindBy(xpath="//*[@id=\'sim-detail\']/form/div[4]/select")
				public WebElementFacade tipotasa;
				
				//Que deseas tasa opcion 0 libre inversion
				@FindBy(xpath="//*[@id=\'sim-detail\']/form/div[5]/div[1]/select")
				public WebElementFacade tipocredito;
				
				//plazo
				@FindBy(xpath="//*[@id=\'sim-detail\']/form/div[7]/div[1]/input")
				public WebElementFacade plazo;
				
				
				//valor a prestar
				@FindBy(xpath="//*[@id=\'sim-detail\']/form/div[7]/div[2]/input")
				public WebElementFacade valorprestamo;
				
				
				//Boton ingresar
				@FindBy(xpath="//*[@id=\'sim-detail\']/form/div[8]/button")
				public WebElementFacade ingresar;
				//*[@id="sim-detail"]/form/div[8]/button
				
				
				
				public void Multiple_Select(String datoPrueba) {
					tiposim.selectByVisibleText(datoPrueba);
				}
				

public void fecha(String datoPrueba) {
	fechanac.click();
	fechanac.clear();
	fechanac.sendKeys(datoPrueba);
	}
				
				
public void Multiple_Select1(String datoPrueba) {
	tipotasa.selectByVisibleText(datoPrueba);
}	
	
public void Multiple_Select2(String datoPrueba) {
	tipocredito.selectByVisibleText(datoPrueba);
}

public void plazo(String datoPrueba) {
	plazo.click();
	plazo.clear();
	plazo.sendKeys(datoPrueba);
	}
		
public void valorcredito(String datoPrueba) {
	valorprestamo.click();
	valorprestamo.clear();
	valorprestamo.sendKeys(datoPrueba);
	}

public void ingresarval() {
	ingresar.click();
	
	}

//validar

//is required
@FindBy(xpath="//*[@id=\"sim-results\"]/div[1]/table/tbody/tr[3]/td[3]")
public WebElementFacade valorval;

public String recoger_val() {
	
	return valorval.getText();
	
}

//is required
@FindBy(className="ng-binding")
public WebElementFacade help;


public void form_sin_errores1() {
assertThat(help.isCurrentlyVisible(), is(false));
}

public void form_con_errores1() {
	assertThat(help.isCurrentlyVisible(), is(true));
}


}
