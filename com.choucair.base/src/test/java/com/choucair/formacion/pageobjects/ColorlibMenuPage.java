package com.choucair.formacion.pageobjects;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

public class ColorlibMenuPage extends PageObject {
	//Menu forms
	@FindBy(xpath="//*[@id='menu']/li[6]/a")
	public WebElement menuForms;
	
	
	
	//submenu Form Validation
	@FindBy(xpath="//*[@id=\'menu\']/li[6]/ul/li[2]/a")
	public WebElement menuFormValidation;
	//*[@id="menu"]/li[6]/ul/li[4]/a
	
	//submenu label Informativo
		@FindBy(xpath="//*[@id=\"content\"]/div/div/div[1]/div/div/header/h5")
		public WebElement lblFormValidation;

		
	//submenu	block validation
		@FindBy(xpath="//*[@id=\'content\']/div/div/div[2]/div/div/header/h5")
		public WebElement lblBlockValidation;

public void menuFormValidation() {
	menuForms.click();
	menuFormValidation.click();
	String strMensaje = lblFormValidation.getText();
	assertThat(strMensaje,containsString("Popup Validation"));
	
}
public void menuBlockValidation() {
	menuForms.click();
	menuFormValidation.click();
	String strMensaje=lblBlockValidation.getText();
	assertThat(strMensaje,containsString("Block Validation"));
}
}