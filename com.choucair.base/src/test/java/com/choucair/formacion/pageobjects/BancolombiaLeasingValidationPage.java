package com.choucair.formacion.pageobjects;

import com.choucair.formacion.steps.BancolombiaLeasingValidationSteps;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class BancolombiaLeasingValidationPage extends PageObject {
BancolombiaLeasingValidationSteps bancolombiaLeasingValidationSteps;

//valor del activo
@FindBy(xpath="//*[@id=\'sim-detail\']/form/div[1]/input")
public WebElementFacade valoractivo;




//plazo contrato
@FindBy(xpath="//*[@id=\'sim-detail\']/form/div[2]/input")
public WebElementFacade plazocontrato;



//porcentaje opcion compra
@FindBy(xpath="//*[@id=\'sim-detail\']/form/div[3]/input")
public WebElementFacade ptjopcioncompra;

//tipo tasa
@FindBy(xpath="//*[@id=\"sim-detail\"]/form/div[4]/select")
public WebElementFacade tipotasa;


//btnsimular
@FindBy(xpath="//*[@id=\"sim-detail\"]/form/div[6]/button")
public WebElementFacade btnsimular;



public void paso_1(String datoPrueba) {
	valoractivo.click();
	valoractivo.clear();
	valoractivo.sendKeys(datoPrueba);
}


public void paso_2(String datoPrueba) {
	plazocontrato.click();
	plazocontrato.clear();
	plazocontrato.sendKeys(datoPrueba);
}

public void paso_3(String datoPrueba) {
	ptjopcioncompra.click();
	ptjopcioncompra.clear();
	ptjopcioncompra.sendKeys(datoPrueba);
	
}
	public void paso_4(String datoPrueba) {
		tipotasa.selectByVisibleText(datoPrueba);
	
}

	public void paso_5() {
		btnsimular.click();
	}

}
