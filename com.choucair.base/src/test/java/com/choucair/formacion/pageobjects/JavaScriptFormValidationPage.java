package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class JavaScriptFormValidationPage extends PageObject{

	//Js Alert
			@FindBy(xpath="//*[@id=\'content\']/div/ul/li[1]/button")
			public WebElementFacade jsalert;
			
			//Js confirm
			@FindBy(xpath="//*[@id=\'content\']/div/ul/li[2]/button")
			public WebElementFacade jsconfirm;
			
			
			//Js prompt
			@FindBy(xpath="//*[@id=\"content\"]/div/ul/li[3]/button")
			public WebElementFacade jsprompt;
	

public void click_alert() {
	jsalert.click();
	getDriver().switchTo().alert().accept();
}

public void click_confirm() {
	jsconfirm.click();
	getDriver().switchTo().alert().dismiss();
}

public void click_prompt(String datoPrueba) {
	jsprompt.click();
	getDriver().switchTo().alert().sendKeys(datoPrueba);
	getDriver().switchTo().alert().accept();
}
}
