package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.grupobancolombia.com/wps/portal/personas")
public class BancolombiaLeasingPage extends PageObject{

	//Boton productos y servicios
	@FindBy(xpath="//*[@id=\'main-menu\']/div[2]/ul[1]/li[3]/a")
	public WebElementFacade productosyserv;
	
	//Leasing
		@FindBy(xpath="//*[@id=\'productosPersonas\']/div/div[1]/div/div/div[2]/div/a")
		public WebElementFacade btnLeasing;
		
		
		//Leasing Habitacional
		@FindBy(xpath="//*[@id=\'category-detail\']/div/div/div[2]/div/div[2]/h2/a")
		public WebElementFacade btnLeasinghab;
		
		//simular canon
				@FindBy(xpath="//*[@id=\'main-content\']/div[4]/div/div/div/div/div[1]/div/div/div[1]/a")
				public WebElementFacade btnsimularcanon;
		
		
		
		public void ingresar_1() {
			productosyserv.click();
		}
	
		public void ingresar_2() {
			btnLeasing.click();
		}
		
		public void ingresar_3() {
			
			btnLeasinghab.click();
		}
	
	public void ingresar_4() {
		btnsimularcanon.click();
	}

}
