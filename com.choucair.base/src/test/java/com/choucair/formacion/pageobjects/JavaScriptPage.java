package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://the-internet.herokuapp.com/")

public class JavaScriptPage extends PageObject{

	
	//link JavaScript
		@FindBy(xpath="//*[@id=\'content\']/ul/li[25]/a")
		public WebElementFacade linkscripts;

	public void ingreso_link_1() {
		linkscripts.click();
		
	}

	
	
}
