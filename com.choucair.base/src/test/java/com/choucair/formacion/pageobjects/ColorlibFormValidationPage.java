package com.choucair.formacion.pageobjects;

import static org.junit.Assert.assertThat;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
 

import static org.hamcrest.Matchers.*;

public class ColorlibFormValidationPage extends PageObject {

	//Campo Required
	@FindBy(id="req")
	public WebElementFacade txtRequired;
	
	//Campo Seleccion de Deporte 1
		@FindBy(xpath="//*[@id='sport']")
		public WebElementFacade cmbSport1;
	
	
	//multiple select
	@FindBy(xpath="//*[@id='sport2']")
	public WebElementFacade cmbSport2;
	
	
	
	//Campo Url
		@FindBy(xpath="//*[@id=\'url1\']")
		public WebElementFacade txtUrl;
		
		//Campo Email
				@FindBy(xpath="//*[@id=\'email1\']")
				public WebElementFacade txtEmail1;
				
				//Campo Password1
				@FindBy(xpath="//*[@id=\'pass1\']")
				public WebElementFacade txtPass1;
				
				//Campo Password2
				@FindBy(xpath="//*[@id=\'pass2\']")
				public WebElementFacade txtPass2;
				
				//Campo MinSize
				@FindBy(xpath="//*[@id=\'minsize1\']")
				public WebElementFacade txtMinsize1;
				
				//Campo MaxSize
				@FindBy(xpath="//*[@id=\'maxsize1\']")
				public WebElementFacade txtMaxsize;
				
				//Campo Number
				@FindBy(xpath="//*[@id=\'number2\']")
				public WebElementFacade txtNumber;
				
				
				//Campo IP
				@FindBy(xpath="//*[@id=\'ip\']")
				public WebElementFacade txtIp;
				
				//Campo Date
				@FindBy(xpath="//*[@id=\'date3\']")
				public WebElementFacade txtDate;
				
				//Campo Date_earlier
				@FindBy(xpath="//*[@id=\'past\']")
				public WebElementFacade txtDateEarlier;
				
				//boton validate
				@FindBy(xpath="//*[@id=\'popup-validation\']/div[14]/input")
				public WebElementFacade btnValidate;
				
				//Globo informativo
				@FindBy(xpath="(//DIV[@class='formErrorContent'])[1]")
				public WebElementFacade globoInformativo;

public void Required(String datoPrueba) {
	txtRequired.click();
	txtRequired.clear();
	txtRequired.sendKeys(datoPrueba);
	}

public void Multiple_Select(String datoPrueba) {
	cmbSport2.selectByVisibleText(datoPrueba);
}

public void Select_Sport(String datoPrueba) {
	cmbSport1.click();
	cmbSport1.selectByVisibleText(datoPrueba);
	}
public void url(String datoPrueba) {
	txtUrl.click();
	txtUrl.clear();
	txtUrl.sendKeys(datoPrueba);
	}

public void email(String datoPrueba){
	txtEmail1.click();
	txtEmail1.clear();
	txtEmail1.sendKeys(datoPrueba);
	}

public void password(String datoPrueba) {
	txtPass1.click();
	txtPass1.clear();
	txtPass1.sendKeys(datoPrueba);
}

public void confirm_password(String datoPrueba) {
	txtPass2.click();
	txtPass2.clear();
	txtPass2.sendKeys(datoPrueba);
	}

public void minumun_field_size(String datoPrueba) {
	txtMinsize1.click();
	txtMinsize1.clear();
	txtMinsize1.sendKeys(datoPrueba);
	}

public void maximun_field_size(String datoPrueba) {
	txtMaxsize.click();
	txtMaxsize.clear();
	txtMaxsize.sendKeys(datoPrueba);
	}
public void number(String datoPrueba) {
	txtNumber.click();
	txtNumber.clear();
	txtNumber.sendKeys(datoPrueba);
	}

public void ip(String datoPrueba) {
	txtIp.click();
	txtIp.clear();
	txtIp.sendKeys(datoPrueba);
	}

public void date(String datoPrueba) {
	txtDate.click();
	txtDate.clear();
	txtDate.sendKeys(datoPrueba);
	}

public void date_earlier(String datoPrueba) {
	txtDateEarlier.click();
	txtDateEarlier.clear();
	txtDateEarlier.sendKeys(datoPrueba);
	}

public void validate() {
	btnValidate.click();
}

public void form_sin_errores() {
assertThat(globoInformativo.isCurrentlyVisible(), is(false));
}

public void form_con_errores() {
	assertThat(globoInformativo.isCurrentlyVisible(), is(true));
}

//segundo formulario

//Campo Required1
@FindBy(xpath="//*[@id=\'required2\']")
public WebElementFacade Required1;

//Campo email2
@FindBy(xpath="//*[@id=\'email2\']")
public WebElementFacade Email2;

//Campo pass
@FindBy(xpath="//*[@id=\'password2\']")
public WebElementFacade Password2;

//Campo confirmpass
@FindBy(xpath="//*[@id=\'confirm_password2\']")
public WebElementFacade Confirmpass;

//Campo DAte
@FindBy(xpath="//*[@id=\'date2\']")
public WebElementFacade Date1;

//Campo url
@FindBy(xpath="//*[@id=\'url2\']")
public WebElementFacade Url1;


//Campo url
@FindBy(xpath="//*[@id=\'url2\']")
public WebElementFacade Url;

//Campo digits
@FindBy(xpath="//*[@id=\'digits\']")
public WebElementFacade Digits;

//Campo Range
@FindBy(xpath="//*[@id=\'range\']")
public WebElementFacade Range1;

//Campo Agree
@FindBy(xpath="//*[@id=\'agree2\']")
public WebElementFacade Agree;

//boton validate
@FindBy(xpath="//*[@id=\'block-validate\']/div[10]/input")
public WebElementFacade btnValidate1;

//is required
@FindBy(className="help-block")
public WebElementFacade help;					 

public void req1(String datoPrueba) {
	Required1.click();
	Required1.clear();
	Required1.sendKeys(datoPrueba);
	}

public void email2(String datoPrueba) {
	Email2.click();
	Email2.clear();
	Email2.sendKeys(datoPrueba);
	}

public void pass(String datoPrueba) {
	Password2.click();
	Password2.clear();
	Password2.sendKeys(datoPrueba);
	}

public void confirmpass(String datoPrueba) {
	Confirmpass.click();
	Confirmpass.clear();
	Confirmpass.sendKeys(datoPrueba);
	}

public void date1(String datoPrueba) {
	Date1.click();

	Date1.sendKeys(datoPrueba);
	}


public void url1(String datoPrueba) {
	Url1.click();
	Url1.clear();
	Url1.sendKeys(datoPrueba);
	}

public void digits(String datoPrueba) {
	Digits.click();
	Digits.clear();
	Digits.sendKeys(datoPrueba);
	}


public void range1(String datoPrueba) {
	Range1.click();
	Range1.clear();
	Range1.sendKeys(datoPrueba);
	}


public void Agree() {
	Agree.click();
	}

public void validate1() {
	btnValidate1.click();
}

public void form_sin_errores1() {
assertThat(help.isCurrentlyVisible(), is(false));
}

public void form_con_errores1() {
	assertThat(help.isCurrentlyVisible(), is(true));
}

}
