package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class ZohoFormValidationPage extends PageObject {

	// Nombre completo
	@FindBy(xpath = "//*[@id=\'namefield\']")
	public WebElementFacade nombre;

	// Correo Electronico
	@FindBy(xpath = "//*[@id=\"email\"]")
	public WebElementFacade email;

	// password
	@FindBy(xpath = "//*[@id=\'dialogRegister\']/div[3]/div/input")
	public WebElementFacade password;

	// seleccionar indicativo_pais
	@FindBy(name = "country_code")
	public WebElementFacade indicativo;

	// numero celular
	@FindBy(xpath = "//*[@id=\'mobile\']")
	public WebElementFacade numerocel;

	// Seleccionar Pais
	@FindBy(xpath = "//*[@id=\'country\']")
	public WebElementFacade pais;

	// terminos y condiciones
	@FindBy(xpath = "//*[@id='dialogRegister']/div[7]/div[2]/label")
	public WebElementFacade terminos;

	// terminos y condiciones
	@FindBy(xpath = "//*[@id=\'signupbtn\']")
	public WebElementFacade ingresar;

	public void ingreso_nombre(String datoPrueba) {
		nombre.click();
		nombre.clear();
		nombre.sendKeys(datoPrueba);
	}

	public void ingreso_correo(String datoPrueba) {
		email.click();
		email.clear();
		email.sendKeys(datoPrueba);
	}

	public void ingreso_pass(String datoPrueba) {
		password.click();
		password.clear();
		password.sendKeys(datoPrueba);

	}

	public void seleccionar_indicativo(String datoPrueba) {

		indicativo.selectByVisibleText(datoPrueba);
	}

	public void ingresar_numero(String datoPrueba) {
		numerocel.click();
		numerocel.clear();
		numerocel.sendKeys(datoPrueba);
	}

	public void seleccionar_pais(String datoPrueba) {

		pais.selectByVisibleText(datoPrueba);
	}

	public void terminos_condiciones() {
		terminos.click();

	}

	public void ingresar() {
		ingresar.click();

	}

	// formulario posterior a ingreso

	// Nombre organizacion
	@FindBy(xpath = "//*[@id=\'orgName\']")
	public WebElementFacade nombreorganizacion;

	// Zona horaria
	@FindBy(xpath = "//*[@id=\'select2-userTimeZone-container\']/span")
	public WebElementFacade zonahoraria;

	// Idioma
	@FindBy(xpath = "//*[@id=\'select2-userTimeZone-container\']/span")
	public WebElementFacade idioma;

	// moneda
	@FindBy(xpath = "//*[@id=\'select2-currencyLocale-container\']")
	public WebElementFacade moneda;

	// boton de ingreso
	@FindBy(xpath = "//*[@id=\'profileDetailBtn2\']")
	public WebElementFacade ingreso2;
	
	// boton de ingreso
		@FindBy(xpath = "//*[@id=\'profileDetailBtn\']")
		public WebElementFacade ingreso3;

	
	
	public void ingreso_3() {
		ingreso3.click();
	}
		
		public void nombre_organizacion(String datoPrueba) {
		nombreorganizacion.click();
		nombreorganizacion.clear();
		nombreorganizacion.sendKeys(datoPrueba);

	}

	public void zona_horaria(String datoPrueba) {
		zonahoraria.selectByVisibleText(datoPrueba);
	}

	public void seleccionar_idioma(String datoPrueba) {
		idioma.selectByVisibleText(datoPrueba);
	}

	public void seleccionar_moneda(String datoPrueba) {
		moneda.selectByVisibleText(datoPrueba);

	}

	public void ingresar_2() {
		ingreso2.click();
	}
}
