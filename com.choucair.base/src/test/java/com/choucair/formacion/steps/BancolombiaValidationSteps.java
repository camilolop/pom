package com.choucair.formacion.steps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import java.util.List;

import javax.swing.JOptionPane;

import com.choucair.formacion.pageobjects.BancolombiaFormValidationPage;
import com.choucair.formacion.pageobjects.BancolombiaPage;
import com.ibm.icu.text.DecimalFormat;

import groovy.transform.ToString;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

public class BancolombiaValidationSteps {

	
	BancolombiaPage bancolombiaPage;
	BancolombiaFormValidationPage bancolombiaFormValidationPage;
	@Step
	
	public void ingreso_pagina_banco(){
		bancolombiaPage.open();
		
	}
@Step

public void ingresar_datos_pagina(List<List<String>> data, int id){
	bancolombiaFormValidationPage.Multiple_Select(data.get(id).get(0).trim());
	bancolombiaFormValidationPage.fecha(data.get(id).get(1).trim());
	bancolombiaFormValidationPage.Multiple_Select1(data.get(id).get(2).trim());
	bancolombiaFormValidationPage.Multiple_Select2(data.get(id).get(3).trim());
	bancolombiaFormValidationPage.plazo(data.get(id).get(4).trim());
	bancolombiaFormValidationPage.valorcredito(data.get(id).get(5).trim());
	
	bancolombiaFormValidationPage.ingresarval();
	



}
@Step
public void validar_datos(List<List<String>> data, int id) {
	
	//obtener los datos de arreglo inicial
	Double p= Double.parseDouble(data.get(id).get(5).trim());
	Double n= Double.parseDouble(data.get(id).get(4).trim());
	Double in= Double.parseDouble(data.get(id).get(6).trim());
	double r,q;
String z;
	//primer tramo calculo formula
	
	q=Math.pow(1+in, n);
	//formula completa
	r=p*(in*(q)/(q-1));
	//capturo valor que obtengo de formulario 
	String x=bancolombiaFormValidationPage.recoger_val();
	
	//formateo los decimales del numero calculado
	 DecimalFormat df = new DecimalFormat("#,###,###,##0.00");

	 z= "$"+ df.format(r);
	//remplazo "." por "," para igualar las cadenas de caracteres
	x=x.replace(".", ",");
	z=z.replace(".", ",");
	 
	 
	//imprimo para verificar
	System.out.print(z);
	System.out.print(x);
	
	
	//comparar las dos cadenas de texto
	assertThat(x, containsString(z));
	
		
}
@Step 
public void validar_globo() {
	
	bancolombiaFormValidationPage.form_sin_errores1();
	bancolombiaFormValidationPage.form_con_errores1();
}



}
