package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.JavaScriptFormValidationPage;
import com.choucair.formacion.pageobjects.JavaScriptPage;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;

public class JavaScriptValidationSteps {

	JavaScriptPage javaScriptPage;
	JavaScriptFormValidationPage javaScriptFormValidationPage;
	@Step
	public void ingreso_pag() {
		javaScriptPage.open();
		
	}

@Step
public void ingreso_link_scr() {
	javaScriptPage.ingreso_link_1();
}

@Step
public void ingreso_eventos(List<List<String>> data, int id) {
	javaScriptFormValidationPage.click_alert();
	javaScriptFormValidationPage.click_confirm();
	javaScriptFormValidationPage.click_prompt(data.get(id).get(0).trim());
}



}

