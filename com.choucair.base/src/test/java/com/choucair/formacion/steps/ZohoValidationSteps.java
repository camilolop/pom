package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.ZohoFormValidationPage;
import com.choucair.formacion.pageobjects.ZohoPage;

import net.thucydides.core.annotations.Step;

public class ZohoValidationSteps {
ZohoPage zohoPage;
ZohoFormValidationPage zohoFormValidationPage;

@Step
public void ingreso_pagina() {
	zohoPage.open();
}

@Step
public void ingreso_datos(List<List<String>> data, int id) {
	
	zohoFormValidationPage.ingreso_nombre(data.get(id).get(0).trim());
	zohoFormValidationPage.ingreso_correo(data.get(id).get(1).trim());
	zohoFormValidationPage.ingreso_pass(data.get(id).get(2).trim());
	//zohoFormValidationPage.seleccionar_indicativo(data.get(id).get(3).trim());
	zohoFormValidationPage.ingresar_numero(data.get(id).get(3).trim());
	zohoFormValidationPage.seleccionar_pais(data.get(id).get(4).trim());
	zohoFormValidationPage.terminos_condiciones();
	zohoFormValidationPage.ingresar();
	/*zohoFormValidationPage.nombre_organizacion(data.get(id).get(5).trim());
	zohoFormValidationPage.zona_horaria(data.get(id).get(6).trim());
	zohoFormValidationPage.seleccionar_idioma(data.get(id).get(7).trim());
	zohoFormValidationPage.seleccionar_moneda(data.get(id).get(8).trim());
	zohoFormValidationPage.ingresar_2();*/
	zohoFormValidationPage.ingreso_3();
	
}


}
