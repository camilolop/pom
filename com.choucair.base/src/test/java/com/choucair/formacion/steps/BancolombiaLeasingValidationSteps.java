package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.BancolombiaLeasingPage;
import com.choucair.formacion.pageobjects.BancolombiaLeasingValidationPage;

import net.thucydides.core.annotations.Step;

public class BancolombiaLeasingValidationSteps {

	
	BancolombiaLeasingValidationPage bancolombiaLeasingValidationPage;
	BancolombiaLeasingPage bancolombiaLeasingPage;
	
	@Step
	public void ingresar_pag() {
		bancolombiaLeasingPage.open();
		bancolombiaLeasingPage.ingresar_1();
		bancolombiaLeasingPage.ingresar_2();
		bancolombiaLeasingPage.ingresar_3();
		bancolombiaLeasingPage.ingresar_4();
	}
	

@Step
public void ingresar_dat(List<List<String>> data, int id) {
	bancolombiaLeasingValidationPage.paso_1(data.get(id).get(0).trim());
	bancolombiaLeasingValidationPage.paso_2(data.get(id).get(1).trim());
	bancolombiaLeasingValidationPage.paso_3(data.get(id).get(2).trim());
	bancolombiaLeasingValidationPage.paso_4(data.get(id).get(3).trim());
	bancolombiaLeasingValidationPage.paso_5();
}

}
