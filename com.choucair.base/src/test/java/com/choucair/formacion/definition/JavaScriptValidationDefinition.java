package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.pageobjects.JavaScriptPage;
import com.choucair.formacion.steps.JavaScriptValidationSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class JavaScriptValidationDefinition {
	
	@Steps
	JavaScriptPage javaScriptPage;
	
	
	@Steps
	JavaScriptValidationSteps javaScriptValidationSteps;
	
	@Given("^que el usuario ingresa a la página de HeroKuapp$")
	public void que_el_usuario_ingresa_a_la_página_de_HeroKuapp() throws Throwable {
	   javaScriptValidationSteps.ingreso_pag();
	    
	}

	@When("^selecciona el link javascript_alerts$")
	public void selecciona_el_link_javascript_alerts() throws Throwable {
	   javaScriptValidationSteps.ingreso_link_scr();
	  
	}

	@And("^interactúa con los controles que hay en esta pantalla$")
	public void interactúa_con_los_controles_que_hay_en_esta_pantalla(DataTable dtDatosForm) throws Throwable {
List<List<String>> data = dtDatosForm.raw();
		
		for(int i=0; i<data.size(); i++) {
			javaScriptValidationSteps.ingreso_eventos(data, i);
	}
	    
	}

	@Then("^Aprende a manejar Alertas tipo JavaScript$")
	public void aprende_a_manejar_Alertas_tipo_JavaScript() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	}
}
