package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.pageobjects.BancolombiaLeasingPage;
import com.choucair.formacion.steps.BancolombiaLeasingValidationSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class BancolombiaLeasingValidationDefinition {

	
	@Steps
	BancolombiaLeasingPage bancolombiaLeasingPage;
	
	@Steps
	BancolombiaLeasingValidationSteps bancolombiaLeasingValidationSteps;

	@Given("^ingreso a la pagina de Bancolombia, Leasing Habitacional$")
	public void ingreso_a_la_pagina_de_Bancolombia_Leasing_Habitacional() throws Throwable {
	bancolombiaLeasingValidationSteps.ingresar_pag();
	}

	@When("^Diligencio Formulario simulacion de canon constante$")
	public void diligencio_Formulario_simulacion_de_canon_constante(DataTable dtDatosForm) throws Throwable {
List<List<String>> data = dtDatosForm.raw();
		
		for(int i=0; i<data.size(); i++) {
			bancolombiaLeasingValidationSteps.ingresar_dat(data, i);
	}
	}

	@Then("^Verifico resultado$")
	public void verifico_resultado() throws Throwable {
	   
	}

}
