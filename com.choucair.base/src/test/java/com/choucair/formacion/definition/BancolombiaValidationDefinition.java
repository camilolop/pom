package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.pageobjects.BancolombiaPage;
import com.choucair.formacion.steps.BancolombiaValidationSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class BancolombiaValidationDefinition {
	
	@Steps
	BancolombiaPage bancolombiaPage;

	@Steps
	BancolombiaValidationSteps bancolombiaValidationSteps;
	
	@Given("^ingreso a la pagina de Bancolombia$")
	public void ingreso_a_la_pagina_de_Bancolombia() throws Throwable {
	bancolombiaValidationSteps.ingreso_pagina_banco();
	}

	@When("^Diligencio Formulario simulacion de credito$")
	public void diligencio_Formulario_simulacion_de_credito(DataTable dtDatosForm) throws Throwable {
		List<List<String>> data = dtDatosForm.raw();
		
		for(int i=0; i<data.size(); i++) {
			bancolombiaValidationSteps.ingresar_datos_pagina(data, i);
	}}
	

	@Then("^Verifico resultado de simulacion$")
	public void verifico_resultado_de_simulacion(DataTable dtDatosForm) throws Throwable {
List<List<String>> data = dtDatosForm.raw();
		
		for(int i=0; i<data.size(); i++) {
			bancolombiaValidationSteps.validar_datos(data, i);
	}
			
	
	
	
	}
	
	@Then("^Verifico que se presente globo informativo de validacion$")
	public void verifico_que_se_presente_globo_informativo_de_validacion() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   bancolombiaValidationSteps.validar_globo();
	}

}
	

