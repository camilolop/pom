package com.choucair.formacion.definition;


import java.util.List;

import com.choucair.formacion.steps.ColorlibFormValidationSteps;
import com.choucair.formacion.steps.PopupValidationSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import net.thucydides.core.annotations.Steps;

public class PopupValidationDefinition {
	
	@Steps
	PopupValidationSteps popupValidationSteps;
	@Steps
	ColorlibFormValidationSteps colorlibFormValidationSteps;
	
	@Given("^Autentico en Colorlib con usuario \"([^\"]*)\" y clave \"([^\"]*)\"$")
	public void autentico_en_Colorlib_con_usuario_y_clave(String Usuario, String Clave)  {
	  popupValidationSteps.login_colorlib(Usuario, Clave);
	}

	@Given("^Ingreso a la funcionalidad Forms Validation$")
	public void ingreso_a_la_funcionalidad_Forms_Validation()  {
		popupValidationSteps.ingresar_form_validation();
		
	 
	}
	@Given("^Ingreso a la funcionalidad block Validation$")
	public void ingreso_a_la_funcionalidad_Block_validation()  {
		popupValidationSteps.ingresar_Block_Validation();
		
	}


	
	@When("^Diligencio Formulario Popup Validation$")
	public void diligencio_Formulario_Popup_Validation(DataTable dtDatosForm)  {
	   List<List<String>> data = dtDatosForm.raw();
	
	for(int i=0; i<data.size(); i++) {
		colorlibFormValidationSteps.diligenciar_popup_datos_tabla(data, i);

		try{
			Thread.sleep(5000);
		}catch(InterruptedException e) {
}}
	}
	
	
	@When("^Diligencio Formulario block validation$")
	public void diligencio_Formulario_Block_Validation(DataTable dtDatosForm) {
		   List<List<String>> data = dtDatosForm.raw();
			
			for(int i=0; i<data.size(); i++) {
				colorlibFormValidationSteps.diligenciar_Block_datos(data, i);

				try{
					Thread.sleep(5000);
				}catch(InterruptedException e) {
		}}	
	
	
}
	
	
	@Then("^Verifico ingreso exitoso$")
	public void verifico_ingreso_exitoso()  {
		colorlibFormValidationSteps.verificar_ingreso_datos_formulario_exitoso();
		
	}
	
	@Then("^Verifico ingreso exitoso1$")
	public void verifico_ingreso_exitoso1()  {
		colorlibFormValidationSteps.verificar_ingreso_datos_formulario_exitoso1();
		
	}
	
	@Then("^verificar que se presente globo informativo de validacion$")
	public void verificar_que_se_presente_globo_informativo_de_validacion()  {
	    // Write code here that turns the phrase above into concrete actions
	   // throw new PendingException();
		colorlibFormValidationSteps.verificar_ingreso_datos_formulario_con_errores();
		
	}

	@Then("^verificar que se presente globo informativo de validacion1$")
	public void verificar_que_se_presente_globo_informativo_de_validacion1()  {
	    // Write code here that turns the phrase above into concrete actions
	   // throw new PendingException();
		colorlibFormValidationSteps.verificar_ingreso_datos_formulario_con_errores1();
		
	}

	
	
	
}
