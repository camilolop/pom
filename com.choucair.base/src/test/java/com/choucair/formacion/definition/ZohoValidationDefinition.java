package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.pageobjects.ZohoPage;
import com.choucair.formacion.steps.ZohoValidationSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class ZohoValidationDefinition {
	
	@Steps
	ZohoPage zohoPage;
	
	@Steps
	ZohoValidationSteps zohoValidationSteps;
	
	@Given("^que el usuario quiere utilizar el CRM Zoho$")
	public void que_el_usuario_quiere_utilizar_el_CRM_Zoho() throws Throwable {
	   zohoValidationSteps.ingreso_pagina();
	    
	}

	@When("^realizo el registro exitoso$")
	public void realizo_el_registro_exitoso(DataTable dtDatosForm) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		List<List<String>> data = dtDatosForm.raw();
		
		for(int i=0; i<data.size(); i++) {
			zohoValidationSteps.ingreso_datos(data, i);
	}
	   
	}

	@Then("^Verifico el acceso a la aplicación$")
	public void verifico_el_acceso_a_la_aplicación() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   
	}
}
