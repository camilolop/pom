#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Formulario Bancolombia

  @tag1
  Scenario: Diligenciamiento exitoso de la simulacion de credito,
  
    Given ingreso a la pagina de Bancolombia
   
    When Diligencio Formulario simulacion de credito
    
    |Simula tu Cuota| 1992-02-24 | Tasa Fija | Crédito de Libre Inversión | 36 | 10000000|0.0135|
    
    Then Verifico resultado de simulacion
    
    |Simula tu Cuota| 1992-02-24 | Tasa Fija | Crédito de Libre Inversión | 36 | 10000000|0.0135|
    
    @tag2
  Scenario: Diligenciamiento exitoso de la simulacion de credito,
  
    Given ingreso a la pagina de Bancolombia
   
    When Diligencio Formulario simulacion de credito
    
    |Simula tu Cuota| 1902-02-24 | Tasa Fija | Crédito de Libre Inversión | 36 | 10000000|0.0135|
    
    Then Verifico que se presente globo informativo de validacion
    
   